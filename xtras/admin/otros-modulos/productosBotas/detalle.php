<?php
$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();
$cat=$row_catalogo['categoria'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['txt'];
$catParentID=$row_CATEGORY['parent'];

$CATEGORY2 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
$row_CATEGORY2 = $CATEGORY2 -> fetch_assoc();
$catParent=$row_CATEGORY2['txt'];


$fechaSQL=$row_catalogo['fecha'];
$segundos=strtotime($fechaSQL);
$fechaUI=date('m/d/Y',$segundos);


echo '
	<div class="uk-width-1-1 margen-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias&cat='.$catParentID.'">'.$catParent.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'">'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['modelo'].'</a></li>
		</ul>
	</div>
	<div class="uk-width-1-2@s margen-v-20">
		<div class="uk-card uk-card-default uk-card-body">
			<div>
				<span class=" uk-text-muted">SKU:</span>
				'.$row_catalogo['sku'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">modelo:</span>
				'.$row_catalogo['modelo'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">material:</span>
				'.$row_catalogo['material'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">precio:</span>
				'.number_format($row_catalogo['precio'],2).'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">descuento:</span>
				'.$row_catalogo['descuento'].'%
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Descripción:</span>
				'.$row_catalogo['txt'].'
			</div>
			<div class="uk-width-1-1 uk-text-right">
				<span class="uk-text-muted">Fecha de captura:</span>
				'.$fechaUI.'
			</div>
		</div>
	</div>
	<div class="uk-width-1-2@s margen-v-20">
		<div class="uk-width-1-1 uk-margin-top">
			<div class="uk-card uk-card-default uk-card-body">
				<div class="uk-width-1-1 uk-margin-top">
					<h4>SEO</h4>
					<span class="uk-text-capitalize uk-text-muted">titulo google:</span>
					'.$row_catalogo['title'].'
				</div>
				<div class="uk-width-1-1">
					<span class="uk-text-capitalize uk-text-muted">descripción google:</span>
					'.$row_catalogo['metadescription'].'
				</div>

				<div class="uk-width-1-1">
					<div class="margen-top-50">
						<h3>Ficha técnica</h3>
						<p class="uk-text-muted">Archivos tipo: PDF</p>
					</div>
					<div class="uk-width-1-1">
						<div id="fileuploaderpdf">
							Cargar
						</div>
					</div>
					<div class="uk-width-1-1 uk-text-center margen-v-20">';
	// Ficha técnica
	$pdfFile='../img/contenido/'.$seccion.'pdf/'.$row_catalogo['pdf'];
	if(strlen($row_catalogo['pdf'])>0 AND file_exists($pdfFile)){
		echo '
						<div class="uk-panel uk-text-center">
							<a class="uk-button uk-button-primary uk-button-large " href="'.$pdfFile.'" target="_blank">
								<span uk-icon="download"></span>
								Descargar PDF
							</a><br><br>
							<button class="uk-button uk-button-danger uk-button-large borrarpdf"><i uk-icon="icon:trash"></i> Eliminar pdf</button>
						</div>';
	}else{
		echo '
						<div class="uk-panel">
							<p class="uk-scrollable-box"><i uk-icon="icon:warning;ratio:4;"></i><br><br>
								Faltan ficha técnica<br><br>
							</p>
						</div>';
	}
	echo '
					</div>
				</div>
			</div>
		</div>
	</div>';


// Relacionar colores
	echo '
	<div class="uk-width-1-1 margen-v-20 uk-text-center">
		<h3>Colores</h3>
		<div class="uk-grid-small uk-flex-center" uk-grid>';
		$CONSULTA = $CONEXION -> query("SELECT * FROM productoscolor ORDER BY txt");
		while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
			$thisID=$rowCONSULTA['id'];
			$imagen   = '../img/contenido/productoscolor/'.$rowCONSULTA['imagen'];
			$colorTxt = (strlen($rowCONSULTA['imagen'])>0 AND file_exists($imagen))?'<div class="uk-border-circle uk-container" style="background:url('.$imagen.');background-size:cover;width:70px;height:70px;border:solid 1px #999;">&nbsp;</div>':'<div class="uk-border-circle uk-container" style="background:'.$rowCONSULTA['txt'].';width:70px;height:70px;border:solid 1px #999;">&nbsp;</div>';
			$CONSULTAX = $CONEXION -> query("SELECT * FROM productoscolorrel WHERE item = $id AND valor = $thisID");
			$numRows=$CONSULTAX->num_rows;
			$estatusIcon=($numRows==0)?'off uk-text-muted':'on uk-text-primary';
			echo '
			<div style="max-width:200px;">
				<div class="uk-card uk-card-default padding-20"> 
					'.$colorTxt.'
					<br>
					'.$rowCONSULTA['name'].'
					<br><br>
					<i class="relajax fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="productoscolorrel" data-id="'.$id.'" data-valor="'.$thisID.'" data-estatus="'.$numRows.'"></i>
				</div>
			</div>
			';
		}
		echo '
		</div>
	</div>';



// Imagen principal
	echo '
	<div class="uk-width-1-1 margen-v-50">
		<h3 class="uk-text-center">Imagen principal</h3>
	</div>
	<div class="uk-width-1-2@s margen-top-50">
		<div class="margen-bottom-50 uk-text-muted">
			Archivo JPG<br><br>
			600 px de ancho<br>
			600 px de alto
		</div>
		<div id="fileuploadermain">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-2@s uk-text-center margen-v-20">'.$fileName;

		$pic='../img/contenido/'.$seccion.'main/'.$row_catalogo['imagen'];
		if(strlen($row_catalogo['imagen'])>0 AND file_exists($pic)){
			echo '
			<div class="uk-panel uk-text-center">
				<a href="'.$pic.'" target="_blank">
					<img src="'.$pic.'" class=" uk-border-rounded margen-top-20">
				</a><br><br>
				<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
			</div>';
		}elseif(strlen($row_catalogo['imagen'])>0 AND strpos($row_catalogo['imagen'], 'ttp')>0){
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<a href="'.$row_catalogo['imagen'].'" target="_blank">
						<img src="'.$row_catalogo['imagen'].'" class=" uk-border-rounded margen-top-20">
					</a>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}else{
			echo '
			<div class="uk-panel uk-text-center">
				<div class="uk-width-1-1">
					<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
						<br><br>
					</p>
				</div>
				<div class="uk-width-1-1">
					Link a la imagen:
				</div>
				<div class="uk-width-1-1">
					<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
				</div>
			</div>';
		}
	echo '
	</div>';


// Galería
	$picTXT='';
	$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id ORDER BY orden,id");
	$numProds=$consultaPIC->num_rows;
	while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {

		$pic='../img/contenido/'.$seccion.'/'.$row_consultaPIC['id'].'-sm.jpg';
		$picLg='../img/contenido/'.$seccion.'/'.$row_consultaPIC['id'].'-orig.jpg';
		if(file_exists($pic)){

			$picTXT.='
					<div id="'.$row_consultaPIC['id'].'">
						<div class="uk-card uk-card-default padding-10">
							<div class="uk-width-1-1 margen-bottom-20">
								<button data-id="'.$row_consultaPIC['id'].'" class="borrarpicgallery uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></button>
							</div>
							<a href="'.$picLg.'" target="_blank">
								<img src="'.$pic.'" class="uk-border-rounded">
							</a>
						</div>
					</div>';
		}else{
			$picTXT.='
					<div id="'.$row_consultaPIC['id'].'">
						<div class="uk-card uk-card-default uk-card-body uk-text-center">
							<button data-id="'.$row_consultaPIC['id'].'" class="borrarpicgallery uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></button>
							<br>
							Imagen rota<br>
							<i uk-icon="icon:ban;ratio:2;"></i>
						</div>
					</div>';
		}
	}


	echo '
	<div class="uk-width-1-1 margen-top-50">
		<h3 class="uk-text-center">Galería</h3>
	</div>
	<div class="uk-width-1-1">
		<div id="fileuploader">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-1 uk-text-center">
		<div uk-grid class="uk-child-width-1-5@l uk-child-width-1-3@s uk-child-width-1-2 uk-grid-small uk-grid-match sortable" data-tabla="'.$seccionpic.'">
			'.$picTXT.'
		</div>
	</div>';


// Buttons
	echo '
	<div style="min-height:300px;">
	</div>

	<div>
		<div id="buttons">
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:pencil;ratio:1.4;"></a> &nbsp;&nbsp;
			<button data-id="'.$row_catalogo['id'].'" class="eliminaprod uk-icon-button uk-button-danger uk-box-shadow-large" uk-icon="icon:trash;ratio:1.4;"></button> &nbsp;&nbsp;
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a> &nbsp;&nbsp;
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';



$scripts='
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=gallery&imagen=\'+data);
			}
		});
		
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=main&imagen=\'+data);
			}
		});

		$("#fileuploaderpdf").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "pdf",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=pdf&imagen=\'+data);
			}
		});
	});

	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&borrarPod&cat='.$cat.'&id="+id);
		} 
	});

	// Borrar foto redes
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpicredes");
		} 
	});

	// Borrar PDF
	$(".borrarpdf").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpdf");
		} 
	});

	$(".borrarpicgallery").click(function() {
		var id = $(this).attr("data-id");
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarpicgallery: 1,
					id: id
				}
			})
			.done(function( msg ) {
				console.log(msg);
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#"+id).fadeOut( "slow" );
			});
		}
	});
	';
