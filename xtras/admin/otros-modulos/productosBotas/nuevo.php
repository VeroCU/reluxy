<?php
if (isset($_GET['cat'])) {
	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catNAME=$row_CATEGORY['txt'];
	$catParentID=$row_CATEGORY['parent'];

	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catParent=$row_CATEGORY['txt'];

	echo '
	<div class="uk-width-1-1 margen-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias&cat='.$catParentID.'">'.$catParent.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'">'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="color-red">Nuevo</a></li>
		</ul>
	</div>';
}else{
	echo '
	<div class="uk-width-1-1 margen-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
		</ul>
	</div>';
}
?>

<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">
	<input type="hidden" name="nuevo" value="1">
	<input type="hidden" name="seccion" value="<?=$seccion?>">

	<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
		<div>
			<label class="uk-text-uppercase" for="sku">sku</label>
			<input type="text" class="uk-input" name="sku" placeholder="sku">
		</div>
		<div>
			<label class="uk-text-capitalize" for="modelo">modelo</label>
			<input type="text" class="uk-input" name="modelo" required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="material">material</label>
			<input type="text" class="uk-input" name="material">
		</div>
		<div>
			<label class="uk-text-capitalize" for="precio">precio</label>
			<input type="number" class="uk-input" name="precio" placeholder="Ej. 1850" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="descuento">descuento</label>
			<input type="number" class="uk-input descuento" name="descuento" placeholder="Ej. 10" value="0" max="100" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="categoria">categoria</label>
			<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>
			<?php
$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
	$parentId=$row_CONSULTA['id'];
	$parentTxt=$row_CONSULTA['txt'];
	echo '
						<optgroup label="'.$parentTxt.'">';
	$CONSULTA1 = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = $parentId ORDER BY txt");
	while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
		if (isset($cat) AND $cat==$row_CONSULTA1['id']) {
			$estatus='selected';
		}else{
			$estatus='';
		}
		echo '
							<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['txt'].'</option>';
	}
	echo '
						</optgroup>';
}
			echo '
			</select>
		</div>';
		?>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="imagen">imagen</label>
			<input type="text" class="uk-input" name="imagen" >
		</div>
		<div class="uk-width-1-1">
			<div class="margen-top-20">
				<label for="txt">Descripción</label>
				<textarea class="editor" name="txt"></textarea>
			</div>
		</div>

		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="title">titulo google</label>
			<input type="text" class="uk-input" name="title" placeholder="Término como alguien nos buscaría">
		</div>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="metadescription">descripción google</label>
			<textarea class="uk-textarea" name="metadescription" placeholder="Descripción explícita para que google muestre a quienes nos vean en las búsquedas"></textarea>
		</div>
		<div class="uk-width-1-1 uk-text-center">
			<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
			<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
		</div>
	</div>
</form>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<?php $scripts='
$(function(){
	$("#datepicker").datepicker();
	$( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
});
'; ?>