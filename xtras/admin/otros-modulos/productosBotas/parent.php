<?php
echo '
	<div class="uk-width-1-1 margen-top-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">'.$seccion.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent" class="color-red">Categorías</a></li>
		</ul>
	</div>


	<div class="uk-width-1-1 margen-v-20">
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
			<thead>
				<tr class="uk-text-muted">
					<th onclick="sortTable(0)" class="pointer uk-text-left">Categoría</th>
					<th width="120px" onclick="sortTable(1)" class="pointer uk-text-center">Subcategorías</th>
					<th width="120px" ></th>
				</tr>
			</thead>
			<tbody class="sortable" data-tabla="'.$seccioncat.'">';

// Obtener subcategorías
$numeroProds=0;
$subcatsNum=0;
$Consulta = $CONEXION -> query("SELECT * FROM $seccioncat WHERE parent = 0 ORDER BY orden");
$numeroSubcats = $Consulta->num_rows;
while ($row_Consulta = $Consulta -> fetch_assoc()) {

	$catId = $row_Consulta['id'];
	$filas = $CONEXION -> query("SELECT * FROM $seccioncat WHERE parent = '$catId'");
	$numeroCats = $filas->num_rows;

	$link='index.php?rand='.rand(1,90000).'&seccion='.$seccion.'&subseccion=categorias&cat='.$catId;

	$borrarSubcat='<a href="javascript:eliminaCat(id='.$catId.')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash"></a>';
	if ($numeroCats>0) {
		$borrarSubcat='<a class="uk-icon-button uk-button-default" uk-tooltip title="No puede eliminar<br>Elimine antes su contenido" uk-icon="icon:trash"></a>';
	}
	echo '
				<tr id="'.$row_Consulta['id'].'">
					<td class="uk-text-left">
						<input type="text" value="'.$row_Consulta['txt'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccioncat.'" data-campo="txt" data-id="'.$row_Consulta['id'].'" tabindex="10" >
					</td>
					<td class="uk-text-center@m">
						<span class="uk-text-muted uk-hidden@m">Subcategorías: </span>
						'.$numeroCats.'
					</td>
					<td class="uk-text-right@m">
						<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
						'.$borrarSubcat.'
					</td>
				</tr>';
}

echo '
			</tbody>
		</table>
	</div>

	<div>
		<div id="buttons">
			<a href="#add" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;" uk-toggle></a>
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>




	<div id="add" uk-modal="center: true" class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

				<input type="hidden" name="nuevacategoria" value="1">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="subseccion" value="'.$subseccion.'">
				<input type="hidden" name="cat" value="0">

				<label for="categoria">Nombre de la categoría</label><br><br>
				<input type="text" name="categoria" class="uk-input" required><br><br>
				<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
				<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
			</form>
		</div>
	</div>
	';


$scripts='
	// Eliminar
	function eliminaCat () { 
		var statusConfirm = confirm("Realmente desea eliminar esta categoria?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&eliminarCat&cat='.$cat.'&id="+id);
		} 
	};

	$(".fichalink").click(function(){
		var id = $(this).attr("data-id");
		$("#fichaid").val(id);
	})
';

