<?php
	date_default_timezone_set('America/Mexico_City');
	$seccion='productos';
	$seccionpic=$seccion.'pic';
	$seccionmain=$seccion.'main';
	$hoy=date('Y-m-d');
	$noPic='../img/design/blank.png';

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Artículo Nuevo      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevo'])){ 
		// Obtenemos los valores enviados
		if(!isset($fallo)){
			$sql = "INSERT INTO $seccion (fecha) VALUES ('$hoy')";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$legendSuccess .= "<br>Producto nuevo";
				$editarNuevo=1;
				$id=$CONEXION->insert_id;
				$subseccion='detalle';
			}else{
				$fallo=1;  
				$legendFail .= "<br>No se pudo agregar a la base de datos - $cat";
			}
		}else{
			$legendFail .= "<br>La categoría o marca están vacíos.";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Artículo Editar      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['editar']) OR isset($editarNuevo)) {
		// Obtenemos los valores enviados

		$fallo=1;  
		$legendFail .= "<br>No se pudo modificar la base de datos";
		foreach ($_POST as $key => $value) {
			if ($key=='txt') {
				$dato = str_replace("'", "&#039;", $value);
			}else{
				$dato = htmlentities($value, ENT_QUOTES);
			}
			$actualizar = $CONEXION->query("UPDATE $seccion SET $key = '$dato' WHERE id = $id");
			$exito=1;
			unset($fallo);
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Categoría Editar Ajax      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if (isset($_POST['editarajaxcat'])) {
		include '../../../includes/connection.php';
		$cat=$_POST['cat'];
		$tabla=$_POST['tabla'];
		$old=$_POST['old'];
		$valor=$_POST['valor'];

		if($actualizar = $CONEXION->query("UPDATE $tabla SET txt = '$valor' WHERE id = $cat")){
			$actualizar = $CONEXION->query("UPDATE $seccion SET $tabla = '$valor' WHERE $tabla = '$old'");
			$mensajeClase='success';
			$mensajeIcon='check';
			$mensaje='Guardado';
		}else{
			$mensajeClase='danger';
			$mensajeIcon='ban';
			$mensaje='No se pudo guardar';
		}
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Categoría Nueva      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['newcat'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['txt'])){ $txt=htmlentities($_POST['txt'], ENT_QUOTES);   }else{	$txt=false; $fallo=1; }
		if (isset($_POST['tabla'])) { $tabla=$_POST['tabla'];   }else{	$tabla=false; $fallo=1; }

		// Actualizamos la base de datos
		if(!isset($fallo)){
			$sql = "INSERT INTO $tabla (txt) VALUES ('$txt')";
			if($insertar = $CONEXION->query($sql)){
				$subcat = $CONEXION->insert_id;
				$exito=1;
			}else{
				$fallo=1;  
				$legendFail .= '<br>No pudo agregarse a la base de datos.<br>tabla: '.$tabla.'<br>txt: '.$txt;
			}
		}else{
			$legendFail .= '<br>El campo está vacío.<br>fallo: '.$fallo.'<br>tabla: '.$tabla.'<br>txt: '.$txt;;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar varios tipos     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminargeneral'])){
		$tabla=$_GET['tabla'];
		if($borrar = $CONEXION->query("DELETE FROM $tabla WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>Eliminado";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto exterior    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarimagen'])){
		$rutaFinal="../img/contenido/".$seccion."logo/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['logo'])>0) {
			// borrar todas las versiones del archivo
			if (file_exists($rutaFinal.$row_CONSULTA['logo'].'.jpg')) {
				$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
				foreach ($array as $key => $value) {
					if (file_exists($rutaFinal.$row_CONSULTA['logo'].$value.'.jpg')) {
						unlink($rutaFinal.$row_CONSULTA['logo'].$value.'.jpg');
					}
				}
			}
			// Borrar de la base de datos
			$actualizar = $CONEXION->query("UPDATE $seccion SET logo = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto Redes    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpicredes'])){
		$rutaFinal="../img/contenido/".$seccion."main/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			// borrar todas las versiones del archivo
			if (file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
				if (file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
					unlink($rutaFinal.$row_CONSULTA['imagen']);
				}
			}
			// Borrar de la base de datos
			$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto galería    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['borrarPic'])){
		$picID=$_GET['picID'];
		$rutaFinal='../img/contenido/'.$seccion.'/';
		$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
		foreach ($array as $key => $value) {
			if (file_exists($rutaFinal.$picID.$value.'.jpg')) {
				unlink($rutaFinal.$picID.$value.'.jpg');
			}
		}
		$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE id = $picID");
		$exito=1;
		$legendSuccess.='<br>Foto eliminada';
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto galería    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['borrarpicgrupo'])){
		$picID=$_GET['picid'];
		$rutaFinal='../img/contenido/grupos/';
		$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
		foreach ($array as $key => $value) {
			if (file_exists($rutaFinal.$picID.$value.'.jpg')) {
				unlink($rutaFinal.$picID.$value.'.jpg');
			}
		}
		$borrar = $CONEXION->query("DELETE FROM grupospic WHERE id = $picID");
		$exito=1;
		$legendSuccess.='<br>Foto eliminada';
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Artículo Borrar      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarPod'])){
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();

		$rutaFinal='../img/contenido/'.$seccion.'logo/';
		if (strlen($row_CONSULTA['logo'])>0 AND file_exists($rutaFinal.$row_CONSULTA['logo'].'.jpg')) {
			$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
			foreach ($array as $key => $value) {
				if (file_exists($rutaFinal.$row_CONSULTA['logo'].$value.'.jpg')) {
					unlink($rutaFinal.$row_CONSULTA['logo'].$value.'.jpg');
				}
			}
		}

		$rutaFinal='../img/contenido/'.$seccion.'main/';
		if (strlen($row_CONSULTA['imagen'])>0 AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
			unlink($rutaFinal.$row_CONSULTA['imagen']);
		}

		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id");
		while($row_CONSULTA = $CONSULTA -> fetch_assoc()){
			$picID=$row_CONSULTA['id'];
			$rutaFinal='../img/contenido/'.$seccion.'/';
			$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
			foreach ($array as $key => $value) {
				if (file_exists($rutaFinal.$picID.$value.'.jpg')) {
					unlink($rutaFinal.$picID.$value.'.jpg');
				}
			}
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccion WHERE id = $id")){
			$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE producto = $id");
			$exito=1;
			$legendSuccess .= "<br>Producto eliminado";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['imagen'])){
		$position=$_GET['position'];

		$xs=1;
		$sm=1;
		$lg=0;


		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_REQUEST['imagen'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if (!isset($fallo)) {
			if(file_exists($rutaInicial.$imagenName)){
				$pic=$id;
				if ($position=='gallery') {
					$rutaFinal='../img/contenido/'.$seccion.'/';
					$sql = "INSERT INTO $seccionpic (producto) VALUES ($id)";
					$insertar = $CONEXION->query($sql);
					$pic=$CONEXION->insert_id;
					$imgFinal=$pic.'.jpg';
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$crear=1;

				}elseif($position=='grupos') {
					$rutaFinal='../img/contenido/grupos/';
					$sql = "INSERT INTO grupospic (producto) VALUES ($cat)";
					$insertar = $CONEXION->query($sql);
					$pic=$CONEXION->insert_id;
					$imgFinal=$pic.'.jpg';
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$crear=1;

				}elseif($position=='main'){
					$rutaFinal='../img/contenido/'.$seccionmain.'/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if (strlen($row_CONSULTA['imagen'])>0 AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '$imgFinal' WHERE id = $id");
					$crear=0;
					$exito=1;

				}elseif($position=='logo'){
					$rutaFinal='../img/contenido/'.$seccion.'logo/';
					$pic=rand(111111111,999999999);
					$imgFinal=$pic.'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$pic=rand(111111111,999999999);
						$imgFinal=$pic.'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if (strlen($row_CONSULTA['logo'])>0 AND file_exists($rutaFinal.$row_CONSULTA['logo'].'.jpg')) {
						$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
						foreach ($array as $key => $value) {
							if (file_exists($rutaFinal.$row_CONSULTA['logo'].$value.'.jpg')) {
								unlink($rutaFinal.$row_CONSULTA['logo'].$value.'.jpg');
							}
						}
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET logo = $pic WHERE id = $id");
					$crear=1;

				}elseif($position=='categoria'){
					$tabla=htmlentities($_GET['tabla']);
					$rutaFinal='../img/contenido/categoria/';
					$pic=rand(111111111,999999999);
					$imgFinal=$pic.'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$pic=rand(111111111,999999999);
						$imgFinal=$pic.'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $tabla WHERE txt = '$cat'");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if (strlen($row_CONSULTA['imagen'])>0 AND file_exists($rutaFinal.$row_CONSULTA['imagen'].'.jpg')) {
						$array = array(0=>'', 1=>'-xs', 2=>'-sm', 3=>'-orig',4=>'-lg');
						foreach ($array as $key => $value) {
							if (file_exists($rutaFinal.$row_CONSULTA['imagen'].$value.'.jpg')) {
								unlink($rutaFinal.$row_CONSULTA['imagen'].$value.'.jpg');
							}
						}
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $tabla SET imagen = $pic WHERE txt = '$cat'");
					$crear=1;
				}
			}else{
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}
		}

		if ($crear==1) {

			$imagenName=$imgFinal;

			$imgAux=$rutaFinal.$imagenName;

			//check extension of the file
			$i = strrpos($imagenName,'.');
			$l = strlen($imagenName) - $i;
			$ext = strtolower(substr($imagenName,$i+1,$l));

			// Leer el archivo para hacer la nueva imagen
			$original = imagecreatefromjpeg($imgAux);

			// Tomamos las dimensiones de la imagen original
			$ancho  = imagesx($original);
			$alto   = imagesy($original);


			if ($xs==1) {
				//  Imagen xs
				$newName=$pic."-xs.jpg";
				$anchoNuevo = 50;
				$altoNuevo  = $anchoNuevo*$alto/$ancho;

				// Creamos la imagen
				$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
				// Copiamos el contenido de la original para pegarlo en el archivo nuevo
				imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
				// Pegamos el contenido de la imagen
				if(imagejpeg($imagenAux,$rutaFinal.$newName,90)){ // 90 es la calidad de compresión
					$exito=1;
				}
			}

			if ($sm==1) {
				//  Imagen sm
				$newName=$pic."-sm.jpg";
				$anchoNuevo = 300;
				$altoNuevo  = $anchoNuevo*$alto/$ancho;

				// Creamos la imagen
				$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
				// Copiamos el contenido de la original para pegarlo en el archivo nuevo
				imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
				// Pegamos el contenido de la imagen
				if(imagejpeg($imagenAux,$rutaFinal.$newName,90)){ // 90 es la calidad de compresión
					$exito=1;
				}
			}

			if ($lg==1) {
				//  Imagen lg
				$newName=$pic."-lg.jpg";
				if ($ancho>$alto) {
					$anchoNuevo = 1000;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;
				}else{
					$altoNuevo  = 1000;
					$anchoNuevo = $altoNuevo*$ancho/$alto;
				}

				// Creamos la imagen
				$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
				// Copiamos el contenido de la original para pegarlo en el archivo nuevo
				imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
				// Pegamos el contenido de la imagen
				if(imagejpeg($imagenAux,$rutaFinal.$newName,90)){ // 90 es la calidad de compresión
					$exito=1;
				}
			}

			if($exito=1){
				$legendSuccess .= "<br>Imagen actualizada";
			}
		}else{
			if(!isset($exito)){
				$fallo=1;
			}
			$legendFail.= '<br>No pudo subirse la imagen';
		}

		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// Borramos las imágenes que estén remanentes en el directorio files
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		$filehandle = opendir($rutaInicial); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != ".gitignore" && $file != ".htaccess" && $file != "thumbnail") {
				if(file_exists($rutaInicial.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaInicial.$file);
				}
			}
		} 
		closedir($filehandle); 
	}






