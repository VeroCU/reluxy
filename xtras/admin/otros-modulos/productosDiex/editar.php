<?php

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();
$cat=$row_catalogo['categoria'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['txt'];
$catParentID=$row_CATEGORY['parent'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catParent=$row_CATEGORY['txt'];


echo '
<div class="uk-width-1-1 margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias">Categorías</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'">'.$catNAME.'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$row_catalogo['titulo'].'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="color-red">Editar</a></li>
	</ul>
</div>

<div class="uk-width-1-1 margen-top-20 uk-form">
	<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);">
		<input type="hidden" name="editar" value="1">
		<input type="hidden" name="seccion" value="'.$seccion.'">
		<input type="hidden" name="subseccion" value="detalle">
		<input type="hidden" name="cat" value="'.$cat.'">
		<input type="hidden" name="id" value="'.$id.'">
		<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
			<div>
				<label class="uk-text-capitalize" for="titulo">Título:</label>
				<input type="text" class="uk-input" name="titulo" value="'.$row_catalogo['titulo'].'" autofocus required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="edad">Codigo SAP:</label>
				<input type="text" class="uk-input" name="edad" value="'.$row_catalogo['edad'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="precio">Precio:</label>
				<input type="text" class="uk-input" name="precio" value="'.$row_catalogo['precio'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="tipo">Tipo de autoparte</label>
				<input type="text" class="uk-input" name="tipo" value="'.$row_catalogo['tipo'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="mar">Marca:</label>
				<input type="text" class="uk-input" name="mar" value="'.$row_catalogo['mar'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="mol">Modelo</label>
				<input type="text" class="uk-input" name="mol" value="'.$row_catalogo['mol'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="ano">Año</label>
				<input type="text" class="uk-input" name="ano" value="'.$row_catalogo['ano'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="existencias">Existencias</label>
				<input type="text" class="uk-input" name="existencias" value="'.$row_catalogo['existencias'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="categoria">categoria</label>
				<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>';

$consultaCat = $CONEXION -> query("SELECT * FROM productoscat WHERE parent != 0 ORDER BY txt");
while ($rowConsultaCat = $consultaCat -> fetch_assoc()) {
if ($cat==$rowConsultaCat['id']) {
	$estatus='selected';
}else{
	$estatus='';
}
$thisName=html_entity_decode($rowConsultaCat['txt']);
echo '
					<option value="'.$rowConsultaCat['id'].'" '.$estatus.'>'.$thisName.'</option>';
}

echo '
				</select>
			</div>
			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="instagram">instagram</label>
				<input type="text" class="uk-input" name="instagram" value="'.$row_catalogo['instagram'].'">
			</div>
			<div class="uk-width-1-1">
				<div class="margen-top-20">
					<label for="txt">Aplicaciones</label>
					<textarea class="editor" name="txt">'.$row_catalogo['txt'].'</textarea>
				</div>
			</div>
			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="title">titulo google</label>
				<input type="text" class="uk-input" name="title" value="'.$row_catalogo['title'].'">
			</div>
			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="metadescription">descripción google</label>
				<textarea class="uk-textarea" name="metadescription">'.$row_catalogo['metadescription'].'</textarea>
			</div>
			<div class="uk-width-1-1 uk-text-center">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</div>
	</form>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

';