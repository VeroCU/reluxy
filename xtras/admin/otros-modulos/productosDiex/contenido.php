<?php 
$pag=(isset($_GET['pag']))?$_GET['pag']:0;
$prodspagina=(isset($_GET['prodspagina']))?$_GET['prodspagina']:20;
$consulta = $CONEXION -> query("SELECT * FROM $seccion ORDER BY categoria");

$numItems=$consulta->num_rows;
$prodInicial=$pag*$prodspagina;

echo '
<div class="uk-width-1-3@s margen-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Productos &nbsp; <span class="uk-text-muted uk-text-lowercase"> &nbsp; <b>'.$numItems.'</b> productos</span></a></li>
	</ul>
</div>

<div id="acciones" class="uk-width-2-3@s uk-text-right margen-v-20">
	<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
		<div>
			<a href="modulos/productos/exportar.php" class="uk-button uk-button-primary" targer="_blank" download="productos.csv"><i uk-icon="download"></i> &nbsp; Exportar</a>
		</div>
		<div>
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=importar&deleteprev=1" class="uk-button uk-button-primary"><i uk-icon="upload"></i> &nbsp; Importar</a>
		</div>
		<div>
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias" class="uk-button uk-button-primary"><i uk-icon="folder"></i> &nbsp; Categorías</a>
		</div>
	</div>
</div>

<div class="uk-width-1-1">
	<div uk-grid class="uk-grid-small uk-child-width-expand@m uk-child-width-1-2">
		<div><label class="pointer"><i uk-icon="search"></i> Título<br><input type="text" class="uk-input search" data-campo="titulo"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Código SAP<br><input type="text" class="uk-input search" data-campo="edad"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Precio<br><input type="text" class="uk-input search" data-campo="precio"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Tipo<br><input type="text" class="uk-input search" data-campo="tipo"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Marca<br><input type="text" class="uk-input search" data-campo="mar"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Modelo<br><input type="text" class="uk-input search" data-campo="mol"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Año<br><input type="text" class="uk-input search" data-campo="ano"></label></div>
	</div>
</div>

<div class="uk-width-1-1">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th style="width:20px;"  ></th>
				<th style="width:auto;"  onclick="sortTable(1)"   class="pointer uk-text-left"> &nbsp;&nbsp; Título</th>
				<th style="width:120px;" onclick="sortTable(2)"   class="pointer uk-text-left"> &nbsp;&nbsp; SAP</th>
				<th style="width:70px;"  onclick="sortTable(3)"   class="pointer uk-text-center">Almacén</th>
				<th style="width:90px;"  onclick="sortTable(4)"   class="pointer uk-text-center">Precio</th>
				<th style="width:90px;"  onclick="sortTable(5)"   class="pointer uk-text-center">Categoría</th>
				<th style="width:90px;"  onclick="sortTable(6)"   class="pointer uk-text-center">Tipo</th>
				<th style="width:90px;"  onclick="sortTable(7)"   class="pointer uk-text-center">Marca</th>
				<th style="width:90px;"  onclick="sortTable(8)"   class="pointer uk-text-center">Modelo</th>
				<th style="width:70px;"  onclick="sortTable(9)"   class="pointer uk-text-center">Año</th>
				<th style="width:90px;"  onclick="sortTable(10)"  class="pointer uk-text-center">Activo</th>
				<th style="width:90px;"  ></th>
			</tr>
		</thead>
		<tbody id="conetent">';

		$consulta = $CONEXION -> query("SELECT * FROM $seccion ORDER BY categoria LIMIT $prodInicial,$prodspagina");
		while ($row_Consulta1 = $consulta -> fetch_assoc()) {
			$prodID=$row_Consulta1['id'];
			$catId=$row_Consulta1['categoria'];

			$CONSULTA4 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catId");
			$row_CONSULTA4 = $CONSULTA4 -> fetch_assoc();
			$categoriaTxt=$row_CONSULTA4['txt'];
			$parent=$row_CONSULTA4['parent'];

			$CONSULTA5 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $parent");
			$row_CONSULTA5 = $CONSULTA5 -> fetch_assoc();
			$marcaTxt=$row_CONSULTA5['txt'];

			$picTxt='';
			$pic='../img/contenido/'.$seccion.'main/'.$row_Consulta1['imagen'];
			if(strlen($row_Consulta1['imagen'])>0 AND file_exists($pic)){
				$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
				$pic=$row_Consulta1['imagen'];
				$picTxt= '
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}


			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];

			$estatusIcon=($row_Consulta1['estatus']==0)?'off uk-text-muted':'on uk-text-primary';

			echo '
			<tr id="'.$prodID.'">
				<td>
					'.$picTxt.'
				</td>
				<td>
					<span class="uk-hidden">'.$row_Consulta1['titulo'].'</span>
					<input class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" value="'.$row_Consulta1['titulo'].'" tabindex="10">
				</td>
				<td>
					<span class="uk-hidden">'.$row_Consulta1['edad'].'</span>
					<input class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="edad" data-id="'.$prodID.'" value="'.$row_Consulta1['edad'].'" tabindex="9">
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['existencias']))).'</span>
					<input class="editarajax uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="existencias" data-id="'.$prodID.'" value="'.$row_Consulta1['existencias'].'" tabindex="8">
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['precio']))).'</span>
					<input class="editarajax uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$row_Consulta1['precio'].'" tabindex="7">
				</td>
				<td>
					'.$categoriaTxt.'
				</td>
				<td>
					<span class="uk-hidden">'.$row_Consulta1['tipo'].'</span>
					<input class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="tipo" data-id="'.$prodID.'" value="'.$row_Consulta1['tipo'].'" tabindex="6">
				</td>
				<td>
					<span class="uk-hidden">'.$row_Consulta1['mar'].'</span>
					<input class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="mar" data-id="'.$prodID.'" value="'.$row_Consulta1['mar'].'" tabindex="5">
				</td>
				<td>
					<span class="uk-hidden">'.$row_Consulta1['mol'].'</span>
					<input class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="mol" data-id="'.$prodID.'" value="'.$row_Consulta1['mol'].'" tabindex="4">
				</td>
				<td>
					<span class="uk-hidden">'.$row_Consulta1['ano'].'</span>
					<input class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="ano" data-id="'.$prodID.'" value="'.$row_Consulta1['ano'].'" tabindex="3">
				</td>
				<td class="uk-text-center@m">
					<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
				</td>
				<td class="uk-text-right@m">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$row_Consulta1['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}

echo '
		</tbody>
	</table>
</div>';

?>



<!-- PAGINATION -->
<div class="uk-width-1-1 padding-top-50">
	<div uk-grid class="uk-flex-center">
		<div>
			<ul class="uk-pagination uk-flex-center uk-text-center">
			<?php
			if ($pag!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag-1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'"><i class="fa fa-lg fa-angle-left"></i> &nbsp;&nbsp; Anterior</a></li>';
			}
			$pagTotal=intval($numItems/$prodspagina);
			$modulo=$numItems % $prodspagina;
			if (($modulo) == 0){
				$pagTotal=($numItems/$prodspagina)-1;
			}
			for ($i=0; $i <= $pagTotal; $i++) { 
				$clase='';
				if ($pag==$i) {
					$clase='uk-badge bg-primary color-white';
				}
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($i).'&prodspagina='.$prodspagina;
				echo '<li><a href="'.$link.'" class="'.$clase.'">'.($i+1).'</a></li>';
			}
			if ($pag!=$pagTotal AND $numItems!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag+1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'">Siguiente &nbsp;&nbsp; <i class="fa fa-lg fa-angle-right"></i></a></li>';
			}
			?>

			</ul>
		</div>
		<div class="uk-text-right" style="margin-top: -10px;">
			<select name="prodspagina" data-placeholder="Productos por página" id="prodspagina" class="chosen-select uk-select" style="width:120px;">
				<?php
				$arreglo = array(5=>5,20=>20,50=>50,100=>100,500=>500,9999=>"Todos");
				foreach ($arreglo as $key => $value) {
					$checked='';
					if ($key==$prodspagina) {
						$checked='selected';
					}
					echo '
					<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
				}
				?>
				
			</select>
		</div>
	</div>
</div><!-- PAGINATION -->




<div style="min-height:300px;">
</div>


<div>
	<div id="buttons">
		<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=nuevo" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<div id="nuevacat" uk-modal="center: true">
	<div class="uk-modal-dialog uk-modal-body">
		<a class="uk-modal-close uk-close"></a>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

			<input type="hidden" name="nuevacategoria" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="categorias">

			<label for="categoria">Nombre de la nueva categoría</label><br><br>
			<input type="text" class="uk-input" name="categoria" tabindex="10" required><br><br>
			<input type="submit" name="send" value="Agregar" tabindex="10" class="uk-button uk-button-primary">
		</form>
	</div>
</div>


<?php 
$scripts='
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&borrarPod&id="+id);
		} 
	});

	$("#prodspagina").change(function(){
		var prodspagina = $(this).val();
		window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&prodspagina="+prodspagina);
	})

	$(".search").keypress(function(e) {
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor);
		}
	});
	';
?>

