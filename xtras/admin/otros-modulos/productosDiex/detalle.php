<?php 

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();
$cat=$row_catalogo['categoria'];

$fechaSQL=$row_catalogo['fecha'];
$segundos=strtotime($fechaSQL);
$fechaUI=date('m/d/Y',$segundos);


$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['txt'];
$catParentID=$row_CATEGORY['parent'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catParent=$row_CATEGORY['txt'];

echo '
<div class="uk-width-1-1 margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias">Categorías</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'">'.$catNAME.'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['titulo'].'</a></li>
	</ul>
</div>
<div class="uk-width-1-2@s margen-v-20">
	<div class="uk-card uk-card-default uk-card-body">
		<div>
			<span class=" uk-text-muted">Código SAP:</span>
			'.$row_catalogo['edad'].'
		</div>
		<div>
			<span class="uk-text-capitalize uk-text-muted">Título:</span>
			'.$row_catalogo['titulo'].'
		</div>
		<div>
			<span class="uk-text-capitalize uk-text-muted">precio:</span>
			'.number_format($row_catalogo['precio'],2).'
		</div>
		<div>
			<span class="uk-text-capitalize uk-text-muted">Aplicaciones:</span>
			'.$row_catalogo['txt'].'
		</div>
		<div class="uk-width-1-1 uk-text-right">
			<span class="uk-text-muted">Fecha de captura:</span>
			'.$fechaUI.'
		</div>
	</div>
</div>
<div class="uk-width-1-2@s margen-v-20">
	<div class="uk-width-1-1 uk-margin-top">
		<div class="uk-card uk-card-default uk-card-body">
			<div class="uk-width-1-1 uk-margin-top">
				<h4>SEO</h4>
				<span class="uk-text-capitalize uk-text-muted">titulo google:</span>
				'.$row_catalogo['title'].'
			</div>
			<div class="uk-width-1-1">
				<span class="uk-text-capitalize uk-text-muted">descripción google:</span>
				'.$row_catalogo['metadescription'].'
			</div>
		</div>
	</div>
	<div class="uk-width-1-1 uk-margin-top">
		<div class="uk-card uk-card-default uk-card-body">
			<div class="uk-width-1-1 uk-margin-top">
				<h4>Datos para el filtro</h4>
				<span class="uk-text-capitalize uk-text-muted">Tipo de autoparte:</span>
				'.$row_catalogo['tipo'].'
			</div>
			<div class="uk-width-1-1">
				<span class="uk-text-capitalize uk-text-muted">Marca vehiculo</span>
				'.$row_catalogo['mar'].'
			</div>
			<div class="uk-width-1-1">
				<span class="uk-text-capitalize uk-text-muted">Modelo vehiculo</span>
				'.$row_catalogo['mol'].'
			</div>
			<div class="uk-width-1-1">
				<span class="uk-text-capitalize uk-text-muted">Año vehiculo</span>
				'.$row_catalogo['ano'].'
			</div>						
		</div>
	</div>
</div>';



// Imagen
echo '
<div class="uk-width-1-1 margen-v-50">
	<hr class="uk-divider-icon">
</div>
<div class="uk-width-1-1">
	<h3 class="uk-text-center">Imagen</h3>
</div>
<div class="uk-width-1-2@s margen-top-50">
	<div class="margen-bottom-50 uk-text-muted">
		Archivo JPG<br><br>
		600 px de ancho<br>
		600 px de alto
	</div>
	<div id="fileuploadermain">
		Cargar
	</div>
</div>
<div class="uk-width-1-2@s uk-text-center margen-v-20">'.$fileName;

	$pic='../img/contenido/'.$seccion.'main/'.$row_catalogo['imagen'];
	if(strlen($row_catalogo['imagen'])>0 AND file_exists($pic)){
		echo '
		<div class="uk-panel uk-text-center">
			<a href="'.$pic.'" target="_blank">
				<img src="'.$pic.'" class="img-responsive uk-border-rounded margen-top-20">
			</a><br><br>
			<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
		</div>';
	}elseif(strlen($row_catalogo['imagen'])>0 AND strpos($row_catalogo['imagen'], 'ttp')>0){
		echo '
		<div class="uk-panel uk-text-center">
			<div class="uk-width-1-1">
				<a href="'.$row_catalogo['imagen'].'" target="_blank">
					<img src="'.$row_catalogo['imagen'].'" class="img-responsive uk-border-rounded margen-top-20">
				</a>
			</div>
			<div class="uk-width-1-1">
				Link a la imagen:
			</div>
			<div class="uk-width-1-1">
				<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
			</div>
		</div>';
	}else{
		echo '
		<div class="uk-panel uk-text-center">
			<div class="uk-width-1-1">
				<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
					<br><br>
				</p>
			</div>
			<div class="uk-width-1-1">
				Link a la imagen:
			</div>
			<div class="uk-width-1-1">
				<input class="editarajax uk-input" data-tabla="productos" data-campo="imagen" data-id="'.$id.'" value="'.$row_catalogo['imagen'].'" tabindex="10" placeholder="Ejemplo: https://image.ibb.co/e2eXT7/Fotolia-141780386-Subscription-Monthly-M.jpg">
			</div>
		</div>';
	}
echo '
</div>

<div style="min-height:300px;">
</div>

<div>
	<div id="buttons">
		<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:pencil;ratio:1.4;"></a>
		<button data-id="'.$row_catalogo['id'].'" class="eliminaprod uk-icon-button uk-button-danger uk-box-shadow-large" uk-icon="icon:trash;ratio:1.4;"></button>
		<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
';



$scripts='
	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=main&imagen=\'+data);
			}
		});
	});

	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&borrarPod&cat='.$cat.'&id="+id);
		} 
	});

	// Borrar foto redes
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpicredes");
		} 
	});

	';
