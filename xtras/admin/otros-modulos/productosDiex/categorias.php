<?php
$cat=1;
echo '

<div class="uk-width-1-1 margen-top-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias" class="color-red">Categorías</a></li>
	</ul>
</div>


<div class="uk-width-medium-1-1 margen-v-20">
	<div class="uk-grid">
		<div class="uk-width-1-1 margen-bottom-50">
			<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle" id="ordenar">
				<thead>
					<tr class="uk-text-muted">
						<th style="width:20px;"  ></th>
						<th style="width:20px;"  onclick="sortTable(1)" class="pointer">ID</th>
						<th style="width:auto;"  onclick="sortTable(2)" class="pointer">Categoría</th>
						<th style="width:100px;" onclick="sortTable(3)" class="pointer uk-text-center">Productos</th>
						<th style="width:120px;" ></th>
					</tr>
				</thead>
				<tbody class="sortable" data-tabla="'.$seccioncat.'">
				';
// Obtener subcategorías
$numeroProds=0;
$subcatsNum=0;
$consulta2 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE parent = $cat ORDER BY orden,id");
$numeroSubcats = $consulta2->num_rows;
while ($rowConsulta2 = $consulta2 -> fetch_assoc()) {

	$categoriaMENU=$rowConsulta2['id'];
	$filas = $CONEXION -> query("SELECT * FROM $seccion WHERE categoria = '$categoriaMENU'");
	$numeroProdsThis = $filas->num_rows;
	$numeroProds+=$numeroProdsThis;
	$row_Filas = $filas -> fetch_assoc();
	$subcatsNum=$row_Filas;

	$picTxt='';
	$pic='../img/contenido/'.$seccion.'cat/'.$rowConsulta2['imagen'];
	if(strlen($rowConsulta2['imagen'])>0 AND file_exists($pic)){
		$picTxt='
			<div class="uk-inline">
				<div class="uk-cover-container uk-border-circle" style="width:50px;height:50px;">
					<img src="'.$pic.'" uk-cover>
				</div>
				<div uk-drop="pos: right-justify">
					<img src="'.$pic.'" class="uk-border-rounded">
				</div>
			</div>';
	}

	$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$categoriaMENU;

	$borrarSubcat='<button data-id="'.$categoriaMENU.'" class="eliminacat uk-icon-button uk-button-danger" uk-icon="icon:trash"></button>';
	if ($numeroProdsThis>0) {
		$borrarSubcat='<button class="uk-icon-button uk-button-default" uk-tooltip title="No puede eliminar<br>Elimine antes su contenido" uk-icon="icon:trash"></a>';
	}
	echo '
					<tr id="'.$rowConsulta2['id'].'">
						<td>
							'.$picTxt.'
						</td>
						<td>
							'.$rowConsulta2['id'].'
						</td>
						<td class="uk-text-left">
							<span class="uk-hidden">'.$rowConsulta2['txt'].'</span><input class="editarajax uk-input uk-form-blank" data-tabla="productoscat" data-campo="txt" data-id="'.$categoriaMENU.'" value="'.$rowConsulta2['txt'].'" tabindex="10">
						</td>
						<td class="uk-text-center">
							'.$numeroProdsThis.'
						</td>
						<td class="uk-text-right">
							<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
							'.$borrarSubcat.'
						</td>
					</tr>';
}


echo '
				</tbody>
			</table>
		</div>
	</div>
</div>

<div style="min-height:300px;">
</div>


<div>
	<div id="buttons">
		<a href="#add" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;" uk-toggle></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>



<div id="add" uk-modal="center: true">
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

			<input type="hidden" name="nuevasubcategoria" value="1">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="subseccion" value="'.$subseccion.'">
			<input type="hidden" name="cat" value="'.$cat.'">

			<label for="categoria">Nombre de la categoría</label><br><br>
			<input type="text" name="categoria" class="uk-input" required><br><br>
			<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
			<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
		</form>
	</div>
</div>
';


$scripts='
	// Eliminar cat
	$(".eliminacat").click(function() {
		var id = $(this).attr(\'data-id\');
		UIkit.modal.confirm("Desea eliminar esto?").then(function() {
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&eliminacat=1&cat="+id);
		});
	});

';

