<?php
	date_default_timezone_set('America/Mexico_City');
	$seccion='productos';
	$seccioncat=$seccion.'cat';
	$seccionpic=$seccion.'pic';
	$seccionmain=$seccion.'main';
	$hoy=date('Y-m-d');

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevo'])){ 
		// Insertar en la base de datos
		$sql = "INSERT INTO $seccion (fecha)".
			"VALUES ('$hoy')";
		if($insertar = $CONEXION->query($sql)){
			$exito=1;
			$legendSuccess .= "<br>Producto nuevo";
			$editarNuevo=1;
			$id=$CONEXION->insert_id;
			$subseccion='detalle';
		}else{
			$fallo=1;  
			$legendFail .= "<br>No se pudo agregar a la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['editar']) OR isset($editarNuevo)) {
		foreach ($_POST as $key => $value) {
			if ($key=='txt' OR $key=='txt1') {
				$dato = trim(str_replace("'", "&#039;", $value));
			}else{
				$dato = trim(htmlentities($value, ENT_QUOTES));
			}
			$actualizar = $CONEXION->query("UPDATE $seccion SET $key = '$dato' WHERE id = $id");
			$exito=1;
			unset($fallo);
		}
		// INSTAGRAM PIC
		if (strlen($_POST['instagram'])>0) {

			if ($texto=file_get_contents($_POST['instagram'])) {
				$texto=strstr($texto,'og:image');
				$texto=strstr($texto,'htt');
				$dato =strstr($texto,'"',true);
				$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '$dato' WHERE id = $id");				
			}else{
				$fallo=1;
				$legendFail='<br>No se pudo leer Instagram';
			}
			
			// Copiar la imagen al server
				//$i = strrpos($fileName,'.');
				//$l = strlen($fileName) - $i;
				//$ext = strtolower(substr($fileName,$i+1,$l));
				//$rutaFinal='../img/contenido/'.$seccionmain.'/';
				//$imgFinal=rand(111111111,999999999).'.'.$ext;
				//if(file_exists($rutaFinal.$imgFinal)){
				//	$imgFinal=rand(111111111,999999999).'.'.$ext;
				//}
				//copy($fileName, $rutaFinal.$imgFinal);
				//$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '$imgFinal' WHERE id = $id");
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarPod'])){
		$rutaIMG="../img/contenido/".$seccionmain."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaIMG.$pic;

		if (strlen($pic)>0 AND file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccion WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>Producto eliminado";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar todos los productos     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['borrartodoslosproductos'])){
		include '../../../includes/connection.php';
		
		$rutaIMG="../../../img/contenido/$seccionmain/";
		$numPics=0;

		$consulta= $CONEXION -> query("SELECT * FROM $seccion");
		while($rowConsulta = $consulta-> fetch_assoc()){
			$pic=$rowConsulta['imagen'];
			$picRow=$rutaIMG.$pic;

			if (strlen($pic)>0 AND file_exists($picRow)) {
				unlink($picRow);
				$numPics++;
			}
		}

		if($borrar = $CONEXION->query("TRUNCATE TABLE $seccion")){
			$mensajeClase='success';
			$mensajeIcon='check';
			$mensaje='Todos los productos borrados<br>'.$numPics.' fotos borradas';
		}else{
			$mensajeClase='danger';
			$mensajeIcon='ban';
			$mensaje='No se pudo guardar';
		}

		echo '{ "msg":"<div class=\'uk-text-center color-blanco bg-'.$mensajeClase.' padding-10 text-lg\'><i class=\'fa fa-lg fa-'.$icono.'\'></i> &nbsp; '.$mensaje.'</div>", "numPics":"'.$numPics.'"}';
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto Redes    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpicredes'])){
		$rutaFinal="../img/contenido/".$seccionmain."/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			unlink($rutaFinal.$row_CONSULTA['imagen']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Categoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevacategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['categoria'])) { $categoria=$_POST['categoria'];   }else{	$categoria=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=(htmlentities($categoria, ENT_QUOTES));

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO $seccioncat (txt) VALUES ('$categoria')";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva categoria";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Subategoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevasubcategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['categoria'])) { $categoria=$_POST['categoria'];   }else{	$categoria=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=htmlentities($categoria, ENT_QUOTES);

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO $seccioncat (txt,parent) VALUES ('$categoria',$cat)";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva subcategoria";
			}else{
				$fallo=1;  
				$legendFail .= "<br>No pudo agregarse a la base de datos ".$seccioncat.'-'.$cat.'-'.$categoria;
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminacat'])){
		$rutaIMG="../img/contenido/".$seccioncat."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		$pic=$rowConsulta['imagenhover'];
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccioncat WHERE id = $cat")){
			$exito=1;
			$legendSuccess .= "<br>Categoria eliminada";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir CSV       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['csvfile'])){

		$rutaInicial="../library/upload-file/php/uploads/";
		$fileName=$_GET['csvfile'];

		$rutaFinal='../img/contenido/'.$subseccion.'/';
		$fileFinal=rand(111111111,999999999).'.csv';
		$rutaFinal=$rutaFinal.$fileFinal;

		$i = strrpos($fileName,'.');
		$l = strlen($fileName) - $i;
		$ext = strtolower(substr($fileName,$i+1,$l));

		// Si no es CSV cancelamos
		if ($ext!='csv') {
			$fallo=1;
			$legendFail='<br>El archivo debe ser CSV';
		}

		if(!file_exists($rutaInicial.$fileName)){
			$fallo=1;
			$legendFail='<br>No se permite refrescar la página.';
		}

		// Guardar en la base de datos
		if (!isset($fallo)) {
			if(copy($rutaInicial.$fileName, $rutaFinal)){
				$legendSuccess.= '<br>Archivo cargado con éxito';
				unlink($rutaInicial.$fileName);
				$gestor = @fopen($rutaFinal, "r");
				while (($bufer = fgets($gestor, 4096)) !== false) {
					$bufer=str_replace('"', '', $bufer);
					if (!isset($showTable)){
						$showTable = 1;
					}else{
						$infoImportar[]=explode(',',trim($bufer));
					}
				}
			    fclose($gestor);
			}else{
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Importar datos       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['importardatos'])){
		$rutaFinal='../img/contenido/'.$subseccion.'/';
		$fileFinal=$_GET['file'];
		$rutaFinal=$rutaFinal.$fileFinal;

		$gestor = @fopen($rutaFinal, "r");
		while (($bufer = fgets($gestor, 4096)) !== false) {
			$bufer=str_replace('"', '', $bufer);
			if (!isset($sql)){
				$sql = "INSERT INTO productos (categoria,edad,titulo,txt,title,metadescription,precio,existencias,estatus,orden,instagram,imagen,tipo,mar,mol,ano) VALUES ";
			}else{
				$infoImportar[]=explode(',',trim($bufer));
			}
		}
	    fclose($gestor);

		$num=0;
		foreach ($infoImportar as $key => $value) {
			if (strlen($value[10])>0) {
				$texto=file_get_contents(trim($value[10]));
				$texto=strstr($texto,'og:image');
				$texto=strstr($texto,'htt');
				$value[11] =strstr($texto,'"',true);
			}

			$num++;
			$sql.='( "'.trim($value[0]).'" , "'.trim($value[1]).'" , "'.trim($value[2]).'" , "'.trim($value[3]).'" , "'.trim($value[4]).'" , "'.trim($value[5]).'" , "'.trim($value[6]).'" , "'.trim($value[7]).'" , "'.trim($value[8]).'" , "'.trim($value[9]).'" , "'.trim($value[10]).'" , "'.trim($value[11]).'" , "'.trim($value[12]).'" , "'.trim($value[13]).'" , "'.trim($value[14]).'" , "'.trim($value[15]).'" ),';
	    }

	    $sql=substr($sql, 0,-1).';';

	    if ($insertar = $CONEXION->query($sql)) {
			$legendSuccess.= '<br>Registros importados: '.$num;
			$exito=1;
		}else{
			$fallo=1;
			$legendFail='<br>No pudo guardar en la base de datos<br>'.$sql2;
		}
	}



//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen       %%%%%%%%%%%%%
	if(isset($_REQUEST['imagen'])){
		$position=$_GET['position'];


		$xs=1;
		$sm=1;
		$lg=1;
		$rutaFinal='../img/contenido/'.$seccion.'/';


		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_REQUEST['imagen'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if (!isset($fallo)) {
			if(file_exists($rutaInicial.$imagenName)){
				$pic=$id;
				if ($position=='gallery') {
					$sql = "INSERT INTO $seccionpic (producto) VALUES ($id)";
					$insertar = $CONEXION->query($sql);
					$pic=$CONEXION->insert_id;
					$crear=1;
				}elseif($position=='pdf'){
					$rutaFinal='../img/contenido/'.$seccion.'pdf/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['pdf']!='' AND file_exists($rutaFinal.$row_CONSULTA['pdf'])) {
						unlink($rutaFinal.$row_CONSULTA['pdf']);
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET pdf = '$imgFinal' WHERE id = $id");
					$crear=0;
				}elseif($position=='categoria'){
					$rutaFinal='../img/contenido/'.$seccioncat.'/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccioncat SET imagen = '$imgFinal' WHERE id = $cat");
					$crear=1;
				}elseif($position=='categoria2'){
					$rutaFinal='../img/contenido/'.$seccioncat.'/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen2']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen2'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen2']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccioncat SET imagen2 = '$imgFinal' WHERE id = $cat");
					$crear=0;
				}elseif($position=='main'){
					$rutaFinal='../img/contenido/'.$seccionmain.'/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '$imgFinal' WHERE id = $id");
					$crear=0;
				}
			}else{
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}
		}

		if (!isset($fallo) and $crear==1) {

			$imagenName=$_REQUEST['imagen'];

			$imgAux=$rutaFinal.$pic."-aux.jpg";

			//check extension of the file
			$i = strrpos($imagenName,'.');
			$l = strlen($imagenName) - $i;
			$ext = strtolower(substr($imagenName,$i+1,$l));

			// Comprobamos que el archivo realmente se haya subido
			if(file_exists($rutaInicial.$imagenName)){

				// Lo movemos al directorio final
				copy($rutaInicial.$imagenName, $imgAux);    

				// Leer el archivo para hacer la nueva imagen
				$original = imagecreatefromjpeg($imgAux);

				// Tomamos las dimensiones de la imagen original
				$ancho  = imagesx($original);
				$alto   = imagesy($original);


				if ($xs==1) {
					//  Imagen xs
					$newName=$pic."-xs.jpg";
					$anchoNuevo = 80;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;

					// Creamos la imagen
					$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($imagenAux,$rutaFinal.$newName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($sm==1) {
					//  Imagen sm
					$newName=$pic."-sm.jpg";
					$anchoNuevo = 400;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;

					// Creamos la imagen
					$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($imagenAux,$rutaFinal.$newName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($lg==1) {
					//  Imagen lg
					$newName=$pic."-lg.jpg";
					if ($ancho>$alto) {
						$anchoNuevo = 1000;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;
					}else{
						$altoNuevo  = 1000;
						$anchoNuevo = $altoNuevo*$ancho/$alto;
					}

					// Creamos la imagen
					$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($imagenAux,$rutaFinal.$newName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($originalPic==0) {
					unlink($imgAux);
				}else{
					rename ($imgAux, $rutaFinal.$pic."-orig.jpg");
				}

				if($exito=1){
					$legendSuccess .= "<br>Imagen actualizada";
				}
			}
		}


		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// Borramos las imágenes que estén remanentes en el directorio files
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		$filehandle = opendir($rutaInicial); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != ".gitignore" && $file != ".htaccess" && $file != "thumbnail") {
				if(file_exists($rutaInicial.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaInicial.$file);
				}
			}
		} 
		closedir($filehandle); 
	}

	if (file_exists('error_log')) {
		unlink('error_log');
	}
