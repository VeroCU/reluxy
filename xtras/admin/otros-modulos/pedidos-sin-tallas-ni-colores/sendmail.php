<?php
	$fallo=0;
	$mensaje="<div class='bg-danger color-blanco'><i class='fa fa-ban'></i>  Ocurrió un error: ";

	$cuerpo = ' 
		<html> 
		<head> 
			<meta content="text/html;charset=UTF-8" http-equiv="Content-Type">
			<title style="margin-left:50px">'.$asunto.'</title>
		</head> 
		<body> 
		<div style="width:100%;background-color:'.$mailBGcolor.';color:#333;padding-bottom:50px;">
			<br /><br /><br />

			<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:760px;background-color:white;">
				<tr>
					<td style="width:700px;">

						<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:700px;color:#333;">
							<tr>
								<td style="text-align:center;padding-top:20px;">
									<img src="'.$logo.'" width="100px;">
								</td>
							</tr>
							<tr>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td style="text-align:center;font-weight:700;">
									'.$asunto.'
								</td>
							</tr>
							<tr>
								<td>
									&nbsp;
								</td>
							</tr>
							<tr>
								<td>
									'.$cuerpoMensaje.'
								</td>
							</tr>
							<tr>
								<td>
									<br /><br />
								</td>
							</tr>

						</table>

					</td>
				</tr>
				<tr style="background-color:#333;">
					<td style="text-align:center;color:white;">
						<br /><br />
						<a href="'.$ruta.'" style="color:white;">www.'.$dominio.'</a>
						<br /><br />
						Tel: '.$telefonoSeparado.'
						<br /><br />
					</td>
				</tr>
			</table>
		</div>
		</body> 
		</html> 
	'; 

		

	if(file_exists('../../../library/phpmailer/class.phpmailer.php')){
		require '../../../library/phpmailer/class.phpmailer.php';
		require '../../../library/phpmailer/class.smtp.php';
	}elseif(file_exists('../../../../library/phpmailer/class.phpmailer.php')){
		require '../../../../library/phpmailer/class.phpmailer.php';
		require '../../../../library/phpmailer/class.smtp.php';
	}else{
		$fallo=1;
		$mensaje.=" Code 00 - No se encontro PHPmailer -";
	}

	// Envío
	if($fallo==0){
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
		//Debug SMTP
		$mail->SMTPDebug = 0;
		//SMTP SECURE
		$mail->SMTPSecure = 'ssl';
		//Set who the message is to be sent from
		$mail->setFrom($RemitenteMail, $Brand);
		//Set an alternative reply-to address
		//$mail->addReplyTo($destinatario1, $Brand);
		//$mail->addReplyTo($email, $nombre);

		if ($debug==1) {
			$mail->addAddress($efra, 'Efra');
			$reciber=$efra;
		}elseif (isset($send2user) AND $send2user==1) {
			$mail->AddBCC($destinatario1, $Brand);
			$mail->addAddress($email, $nombre);
			$reciber=$destinatario1.' y '.$email;
		}elseif (isset($send2user) AND $send2user==2) {
			$mail->addAddress($email, $nombre);
			$reciber=$email;
		}else{
			$mail->addAddress($destinatario1, $Brand);
			$reciber=$destinatario1;
			if (strlen($destinatario2)>0) {
				$mail->addAddress($destinatario2, $Brand);
				$reciber.='y '.$destinatario2;
			}
		}

		//CONTENT HTML & UTF-8
		$mail->IsHTML(true);
		$mail->CharSet = 'UTF-8';
		//Set the subject line
		$mail->Subject = $asunto;
		//CONTENT BODY
		$body = $cuerpo;
		//CONVER HTML BODY
		$mail->MsgHTML($body);
		//BODY MAIL
		$mail->Body = $body;
		//send the message, check for errors
		if($mail->Send()){
			$mensaje="<div class='bg-success colo-blanco'><i class='fa fa-check'></i> Correo enviado";
		}else{
			$mensaje.="<br>No se pudo enviar<br>Codigo: 12<br>Brand: $Brand <br>Dominio: $dominio <br>Remitente: $RemitenteMail <br>Cliente: $nombre";
		}
	}
	$mensaje.='</div>';
