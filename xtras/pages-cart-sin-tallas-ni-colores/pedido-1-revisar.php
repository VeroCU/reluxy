<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>

<?=$header?>

<div class="uk-container uk-container-center">
  <form method="post" action="<?=$rutaEstaPagina?>">
    <input type="hidden" name="actualizarcarro" value="1">

    <div class="uk-width-1-1 margin-top-50">
      <div class="uk-panel uk-panel-box">
        <h3 class="uk-text-center"><i class="uk-icon uk-icon-small uk-icon-check-square-o"></i> &nbsp; Productos y cantidades:</h3>
        <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
          <thead>
            <tr>
              <td width="50px"></td>
              <td >Producto</td>
              <td width="100px" class="uk-text-right">Cantidad</td>
              <td width="100px" class="uk-text-right">Precio</td>
              <td width="100px" class="uk-text-right">Importe</td>
            </tr>
          </thead>
          <tbody>

          <?php
          $subtotal=0;
          $num=0;
          if(isset($_SESSION['carro'])){
            foreach ($arreglo as $key) {

              $prodId=$key['Id'];
              
              $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
              $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
              
              $link=$prodId.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'-.html';

              $importe=$row_CONSULTA1['precio']*$key['Cantidad'];
              $subtotal+=$importe;

              echo '
              <tr>
                <td>
                  <span class="quitar uk-icon-button uk-button-danger" data-id="'.$prodId.'"><i uk-icon="icon:trash"></i></span><br>
                </td>
                <td>
                  <a href="#pics" uk-scroll class="color-general">'.$row_CONSULTA1['sku'].' - '.$row_CONSULTA1['titulo'].'</a>
                </td>
                <td class="uk-text-right">
                  <input type="number" name="cantidad'.$num.'" min="0" value="'.$key['Cantidad'].'"  class="cantidad uk-input uk-form-width-small uk-text-right" tabindex="9" required>
                </td>
                <td class="uk-text-right">
                  '.number_format($row_CONSULTA1['precio'],2).'
                </td>
                <td class="uk-text-right">
                  '.number_format($importe,2).'
                </td>
              </tr>';

              $num++;
            }

            $envio=$shipping*$carroTotalProds;
            $subtotal=$subtotal+$envio+$shippingGlobal;
            $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
            $total=$subtotal+$iva;

            if ($total>0) {
              if ($shippingGlobal>0) {
                echo '
                <tr>
                  <td style="text-align: left;" colspan="2">
                    Envío global
                  </td>
                  <td style="text-align: right; ">
                    1
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($shippingGlobal,2).'
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($shippingGlobal,2).'
                  </td>
                </tr>';
              }
              if ($shipping>0) {
                echo '
                <tr>
                  <td style="text-align: left; " colspan="2">
                    Envío por pieza
                  </td>
                  <td style="text-align: right; ">
                    '.$carroTotalProds.'
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($shipping,2).'
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($envio,2).'
                  </td>
                </tr>';
              }

              if($taxIVA>0){
                echo '
                <tr>
                  <td colspan="4" class="uk-text-right">
                    Subtotal
                  </td>
                  <td class="uk-text-right">
                    '.number_format($subtotal,2).'
                  </td>
                </tr>
                <tr>
                  <td colspan="4" class="uk-text-right">
                    IVA
                  </td>
                  <td class="uk-text-right">
                    '.number_format($iva,2).'
                  </td>
                </tr>
                <tr>
                  <td colspan="4" class="uk-text-right">
                    Total
                  </td>
                  <td class="uk-text-right">
                    '.number_format($total,2).'
                  </td>
                </tr>
                ';
              }else{
                echo '
                <tr>
                  <td colspan="4" class="uk-text-right">
                    Total
                  </td>
                  <td class="uk-text-right">
                    '.number_format($subtotal,2).'
                  </td>
                </tr>';
              }
            }
          }
    echo '
          </tbody>
        </table>
      </div>
    </div>

    <div style="min-height: 50px;">
    </div>';


    if ($carroTotalProds>0) {
      echo '
    <div class="uk-width-1-1 uk-text-center margen-v-50">
      <span class="emptycart uk-button uk-button-large uk-button-default"><i uk-icon="icon:trash"></i> &nbsp; Vaciar carrito</span>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <button class="uk-button uk-button-personal uk-button-large uk-hidden" id="actualizar">Actualizar &nbsp; <i uk-icon="icon:refresh;ratio:1.5;"></i></button>
      <a href="Revisar_datos_personales" class="uk-button uk-button-large uk-button-personal" id="siguiente">Continuar &nbsp; <i uk-icon="icon:arrow-right;ratio:1.5;"></i></a>
    </div>';
    }else{
      echo '
    <div class="uk-width-1-1 uk-text-center margen-v-50">
      <div class="uk-alert uk-alert-danger text-xl">El carro está vacío</div>
    </div>';
    }
    ?>

  </form>

  <div style="min-height: 100px;">
  </div>

  <div uk-grid id="pics">
  <?php
  if(isset($_SESSION['carro'])){
    foreach ($arreglo as $key) {

      $prodId=$key['Id'];
      
      $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
      $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
      
      $link=$prodId.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'_.html';

      $noPic='img/design/camara.jpg';
      $consultaImg = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $prodId");
      $row_imgs = $consultaImg -> fetch_assoc();
      $img =$row_imgs['id'];
      $pic='img/contenido/productos/'.$img.'.jpg';
      
      $picHtml=(file_exists($pic) AND strlen($img)>0)?$pic:$noPic;
      $picHtml=(strpos($pic, 'ttp')>0)?$img:$picHtml;
      echo '
      <div>
        <a href="'.$link.'" target="_blank">
          <div style="max-width:300px;">
            <img src="'.$picHtml.'" class="max-height-300px"> 
          </div>
          <div class="uk-card uk-card-body uk-card-default uk-width-1-1 uk-text-center" style="max-width:300px;">
            '.$row_CONSULTA1['titulo'].'
          </div>
        </a>
      </div>
      ';

      $num++;
    }
  }
  ?>
  
  </div> <!-- grid -->
</div> <!-- container -->

<div class="uk-width-1-1 uk-text-center margen-top-50">
  &nbsp;
</div>

<?=$footer?>

<?=$scriptGNRL?>

<script type="text/javascript">
  $(".quitar").click(function(){
    var id = $(this).data("id");
    $.post("ajaxcart",
    {
      id: id,
      cantidad: 0,
      removefromcart: 1
    })
    .done(function(msg) {
      //console.log(msg);
      location.reload();
    });
  })

  $(".emptycart").click(function(){
    $.ajax({
      method: "POST",
      url: "emptycart",
      data: { 
        emptycart: 1
      }
    })
    .done(function() {
      location.reload();
    });
  })

  $(".cantidad").keyup(function() {
    var inventario = $(this).attr("data-inventario");
    var cantidad = $(this).val();
    inventario=1*inventario;
    cantidad=1*cantidad;
    if(inventario<=cantidad){
      $(this).val(inventario);
    }
    $("#actualizar").removeClass("uk-hidden");
    $("#siguiente").addClass("uk-hidden");
  })

  $(".cantidad").focusout(function() {
    var inventario = $(this).attr("data-inventario");
    var cantidad = $(this).val();
    console.log(cantidad);
    inventario=1*inventario;
    cantidad=1*cantidad;
    if(inventario<=cantidad){
      console.log(inventario*2+" - "+cantidad);
      $(this).val(inventario);
    }
    $("#actualizar").removeClass("uk-hidden");
    $("#siguiente").addClass("uk-hidden");
  })
</script>

</body>
</html>